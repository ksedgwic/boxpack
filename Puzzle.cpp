// Copyright (c) 2005 Bonsai Software, Inc.  All rights reserved.

/// @file Puzzle.cpp
/// @author Ken Sedgwick

#if defined(WIN32)
#include <windows.h>
#endif

#include <cmath>
#include <iostream>
#include <iomanip>

#include <GL/gl.h>
#include <GL/glu.h>

#if defined(WIN32)
#include <wx/msw/winundef.h>
#endif

#include "wx/wx.h"
#include "wx/app.h"
#include "wx/thread.h"
#include "wx/event.h"
#include "wx/glcanvas.h"

#include "Puzzle.h"

using namespace std;

namespace {
    struct BoxLess
    {
        inline bool operator() (Box const * a, Box const * b)
        {
            return *a < *b;
        }
    };

    unsigned const SLEEP = 0;   // Number of mSec to pause.

    class MyThread : public wxThread
    {
    private:
        Puzzle *	m_puzzle;

    public:
        MyThread(Puzzle * puzzle)
            : wxThread(wxTHREAD_JOINABLE)
            , m_puzzle(puzzle)
        {}

        virtual ExitCode Entry()
        {
            m_puzzle->solve();
            return 0;
        }
    };
}

Puzzle::Puzzle()
    : m_running(false)
    , m_positions(0)
{
    m_container = new Box(0, 114, 70, 44);
    m_boxes.push_back(new Box(1, 70, 44, 39));
    m_boxes.push_back(new Box(2, 70, 44, 15));
    m_boxes.push_back(new Box(3, 44, 35, 30));
    m_boxes.push_back(new Box(4, 70, 30, 22));
    m_boxes.push_back(new Box(5, 35, 44, 15));
    m_boxes.push_back(new Box(6, 70, 22, 15));
    m_boxes.push_back(new Box(7, 35, 30, 22));
    m_boxes.push_back(new Box(8, 35, 22, 15));
    m_boxes.push_back(new Box(9, 35, 22, 15));

    m_container->pos(0, 0, 0);
    m_root = NULL;

    unsigned volsum = 0;
    for (unsigned i = 0; i < m_boxes.size(); ++i)
        volsum += m_boxes[i]->volume();

    cerr << "The volume of container is " << m_container->volume() << endl;
    cerr << "The total of the blocks is " << volsum << endl;
}

Puzzle::~Puzzle()
{
    stop();
}

void
Puzzle::start()
{
    if (m_running)
        return;

    m_running = true;

    m_thread = new MyThread(this);
    m_thread->Create();
    m_thread->Run();
}

void
Puzzle::stop()
{
    if (!m_running)
        return;

    m_running = false;

    m_thread->Wait();
}

uint64_t
Puzzle::draw() const
{
    m_pzmutex.Lock();

    glPushMatrix();
    float x0 = float(m_container->m_w) * Box::SCALE / 2;
    float y0 = float(m_container->m_h) * Box::SCALE / 2;
    float z0 = float(m_container->m_d) * Box::SCALE / 2;
    glTranslatef(-x0, -y0, -z0);

    {
        glPushMatrix();
        glTranslatef(0.0, 0.0, 0.0);

        glColor4f(0.0f, 0.0f, 0.0f, 0.2f);
        glLineWidth(1.0);

        float w = m_container->m_w * Box::SCALE;
        float h = m_container->m_h * Box::SCALE;
        float d = m_container->m_d * Box::SCALE;

        glBegin(GL_LINES);

        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(  w, 0.0, 0.0);

        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(0.0,   h, 0.0);

        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(0.0, 0.0,   d);

        glVertex3f(  w, 0.0, 0.0);
        glVertex3f(  w,   h, 0.0);

        glVertex3f(  w, 0.0, 0.0);
        glVertex3f(  w, 0.0,   d);

        glVertex3f(0.0,   h, 0.0);
        glVertex3f(  w,   h, 0.0);

        glVertex3f(0.0,   h, 0.0);
        glVertex3f(0.0,   h,   d);

        glVertex3f(0.0, 0.0,   d);
        glVertex3f(  w, 0.0,   d);

        glVertex3f(0.0, 0.0,   d);
        glVertex3f(0.0,   h,   d);

        glVertex3f(  w,   h, 0.0);
        glVertex3f(  w,   h,   d);

        glVertex3f(  w, 0.0,   d);
        glVertex3f(  w,   h,   d);

        glVertex3f(0.0,   h,   d);
        glVertex3f(  w,   h,   d);

        glEnd();
        glPopMatrix();
    }

    if (m_root)
        m_root->draw();

    glPopMatrix();

    uint64_t positions = m_positions;

    m_pzmutex.Unlock();

    return positions;
}

void
Puzzle::solve()
{
    // In general the solver always has the mutex locked.  It releases
    // it at carefully choosen points during the place-traversal
    // recursion so the drawing thread can get in and draw.
    m_pzmutex.Lock();

    place();

    m_running = false;

    m_pzmutex.Unlock();

    cerr << "SOLVER DONE" << endl;

    if (m_root)
    {
        cerr << "Box             X    Y    Z           W    H    D" << endl
             << "-------------------------------------------------" << endl;
             // "1        pos:   0,   0,   0     sz:  39,  69,  43"

        m_root->dump();
    }
}

bool
Puzzle::place()
{
    // When a solution is found we return "true" all the way back up
    // to the top caller.

    if (!m_running)
        return false;

    if (m_boxes.empty())
    {
        cerr << "SUCCESS!" << endl;
        return true;
    }

    for (unsigned i = 0; i < m_boxes.size(); ++i)
    {
        // Remove the targeted piece from the m_boxes collection.
        Box * bp = m_boxes[i];
        m_boxes.erase(remove(m_boxes.begin(), m_boxes.end(), bp),
                      m_boxes.end());

        // Put it on the used list.
        m_used.push_back(bp);

        // Try it in all available positions.
        if (!m_root)
        {
            m_root = bp;
            bp->pos(0, 0, 0);

            // Try all orientations of this piece.
            for (unsigned i = 0; i < 6; ++i)
            {
                if (fits())
                {
                    // Give the GUI thread a chance here.
                    m_pzmutex.Unlock();
                    if (SLEEP)
                        wxThread::Sleep(SLEEP);
                    m_pzmutex.Lock();

                    // Recurse and try other pieces.
                    if (place())
                        return true;
                }
                else
                {
                    // Count failed positions.
                    m_positions++;
                }

                // Flip the piece to another orientation.
                bp->flip(i % 3);
            }

            m_root = NULL;
        }
        else
        {
            if (traverse(m_root, bp))
                return true;
        }

        // Take it back off the used list.
        m_used.pop_back();

        // Put it back in the list.
        m_boxes.push_back(bp);
        sort(m_boxes.begin(), m_boxes.end(), BoxLess());
    }

    return false;
}

bool
Puzzle::traverse(Box * subroot, Box * bp)
{
    // When a solution is found we return "true" all the way back up
    // to the top caller.

    if (!m_running)
        return false;

    // Check each of the children of the subroot.
    for (unsigned i = 0; i < 3; ++i)
    {
        // Do we have a child in this position?
        if (subroot->m_child[i])
        {
            // Traverse the child.
            if (traverse(subroot->m_child[i], bp))
                return true;
        }
        else
        {
            // Try the piece out in this position.
            subroot->attach(i, bp);

            // Try all orientations of this piece.
            for (unsigned j = 0; j < 6; ++j)
            {
                if (fits())
                {
                    // Give the GUI thread a chance here.
                    m_pzmutex.Unlock();
                    if (SLEEP)
                        wxThread::Sleep(SLEEP);
                    m_pzmutex.Lock();

                    // Recurse and try other pieces.
                    if (place())
                        return true;
                }
                else
                {
                    // Count failed positions.
                    m_positions++;
                }

                // Flip the piece to another orientation.
                bp->flip(j % 3);
            }

            // We are done trying with this piece here.
            subroot->detach(i);
        }
    }

    return false;
}

bool
Puzzle::fits() const
{
    Box * recent = m_used.back();

    // Needs to fit in the container.
    if (!m_container->contains(*recent))
        return false;

    // Does the most recent piece collide with a previous?
    for (unsigned i = 0; i < m_used.size() - 1; ++i)
        if (recent->intersects(*(m_used[i])))
            return false;

    return true;
}

// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:
