WXCXXFLAGS = $(shell wx-config --cxxflags)
WXLIBS = $(shell wx-config --libs)

# CPPFLAGS += -O3 -Wall -g -march=i686 $(WXCXXFLAGS) -DLINUX
CPPFLAGS += -O3 -Wall -g $(WXCXXFLAGS) -DLINUX -DNDEBUG

SRCS = 		\
			AppFrame.cpp \
			Puzzle.cpp \
			Box.cpp \
			Trackball.cpp \
			$(NULL)

PROG = boxpack

LIBS += $(WXLIBS) -lwx_gtk3u_gl-3.0 -lGLU -lGL

OBJS = $(SRCS:%.cpp=%.o)

boxpack: $(OBJS)
	$(CXX) -o $@ $(LDFLAGS) $(OBJS) $(LIBS)

clean:
	rm -f $(PROG) $(OBJS)

BoxPack.o:		BoxPack.cpp Trackball.h BoxSet.h

BoxSet.o:		BoxSet.h BoxSet.cpp

Trackball.o:	Trackball.h Trackball.cpp

# Local Variables:
# mode: Makefile
# tab-width: 4
# End:
