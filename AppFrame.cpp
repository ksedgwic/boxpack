// Copyright (c) 2005 Bonsai Software, Inc.  All rights reserved.

/// @file AppFrame.cpp
/// @author Ken Sedgwick

#if defined(WIN32)
#include <windows.h>
#endif

#include <iomanip>
#include <sstream>
#include <string>
#include <sstream>

#include <GL/gl.h>
#include <GL/glu.h>

#if defined(WIN32)
#include <wx/msw/winundef.h>
#endif

#include <wx/wx.h>
#include <wx/glcanvas.h>

#include "Puzzle.h"
#include "Trackball.h"

using namespace std;

namespace {
    unsigned const UPDMSEC = 10;
}

enum
{
	// Close application.
	ID_EXIT = 1,

	// View Menu
	ID_MENU_VIEW = 200,
	ID_MENU_VIEW_GRID,
	ID_MENU_VIEW_AXIS,

    // Timer
    ID_TIMER = 1001,
};

class GLCanvas : public wxGLCanvas
{
private:
    bool			m_init;
    int				m_mouse_x;
    int				m_mouse_y;
    float			m_rot_quat[4];
    Puzzle *		m_puzzle;
    wxTimer *		m_timer;
    wxStatusBar *	m_statusbar;

public:
    GLCanvas(wxFrame * parent)
        : wxGLCanvas(parent,
                     wxID_ANY,
                     NULL,
                     wxDefaultPosition,
                     wxDefaultSize,
                     0,
                     wxGLCanvasName,
                     wxNullPalette)
        , m_init(false)
        , m_mouse_x(-1)
        , m_mouse_y(-1)
        , m_puzzle(new Puzzle)
        , m_statusbar(NULL)
    {
        // Empirically determined pleasing viewing angle.
        m_rot_quat[0] = 0.24178f;
        m_rot_quat[1] = 0.515189f;
        m_rot_quat[2] = 0.753782f;
        m_rot_quat[3] = 0.328535f;

        m_timer = new wxTimer(this, ID_TIMER);
        m_timer->Start(UPDMSEC);

        m_puzzle->start();
    }

    virtual ~GLCanvas()
    {
        delete m_timer;
    }

    void OnPaint(wxPaintEvent & event)
    {
        wxPaintDC dc(this);
        SetCurrent(this);
        // init OpenGL once, but after SetCurrent
        if (!m_init)
        {
            initgl();
            m_init = true;
        }
        set_view_port();
        render();
        glFlush();
        SwapBuffers();
        event.Skip();
    }

    void OnSize(wxSizeEvent & event)
    {
        // // this is also necessary to update the context on some platforms
        // wxGLCanvas::OnSize(event);

        set_view_port();
    }

    void OnEraseBackground(wxEraseEvent & event)
    {
        event.Skip();
    }

    void OnEnterWindow(wxMouseEvent & event)
    {
        SetFocus();
        event.Skip();
    }

    void OnMouse(wxMouseEvent & event)
    {
        if (event.Dragging())
        {
            wxSize sz(GetClientSize());
            float quat[4];
            Trackball::calc_quat(quat,
                                 (2.0 * m_mouse_x - sz.x) / sz.x,
                                 (sz.y - 2.0 * m_mouse_y) / sz.y,
                                 (2.0 * event.GetX() - sz.x) / sz.x,
                                 (sz.y - 2.0 * event.GetY()) / sz.y);
            Trackball::add_rot_quats(quat, m_rot_quat, m_rot_quat);

            Refresh(false); // prevent flashing
        }
        m_mouse_x = event.GetX();
        m_mouse_y = event.GetY();
    }

    void OnTimer(wxTimerEvent& WXUNUSED(event))
    {
        Refresh(FALSE);

        // Turn off the time-based refresh when the puzzle stops
        // running.
        if (!m_puzzle->is_running())
            m_timer->Stop();
    }

    void set_view_port()
    {
        // set GL viewport
        int width;
        int height;
        GetClientSize(&width, &height);
        {
            SetCurrent(this);
            glViewport(0, 0, (GLint)width, (GLint)height);
        }
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(45.0, (double)width / (double)height, 0.1, 100.0);
        glMatrixMode(GL_MODELVIEW);
    }

    void render()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        glTranslatef(0.0, 0.0, -15.0); // eye position

        // Rotation of scene.
        float m[4][4];
        Trackball::build_rot_matrix(m, m_rot_quat);
        glMultMatrixf(&(m[0][0]));

        // Draw scene.
        uint64_t positions = m_puzzle->draw();

        // Update the position counter.
        if (m_statusbar)
        {
            // Insert ',' in the counter value.
            ostringstream valstrm;
            valstrm << positions;
            string::reverse_iterator it;
            unsigned ctr = 0;
            string instr = valstrm.str();
            string valstr;
            for (it = instr.rbegin(); it != instr.rend(); ++it)
            {
                if (ctr && ctr % 3 == 0)
                    valstr.insert(valstr.begin(), ',');
                valstr.insert(valstr.begin(), *it);
                ctr++;
            }

            // Write the number of positions considered.
            ostringstream ostrm;
            ostrm << valstr << " positions considered";
            m_statusbar->SetStatusText(wxString(ostrm.str().c_str(),
                                                wxConvUTF8));
        }
    }

    void initgl()
    {
        SetCurrent(this);
        glClearColor(0.8f, 0.8f, 0.8f, 0.0f);

        // flags
        glFrontFace(GL_CCW);
        glDisable(GL_CULL_FACE);
        // glEnable(GL_DEPTH_TEST);

        // nice shading
        glShadeModel(GL_SMOOTH);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

        glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
        // glEnable(GL_COLOR_MATERIAL);
    }

    void stop()
    {
        m_puzzle->stop();
    }

    void set_status_bar(wxStatusBar * statusbar)
    {
        m_statusbar = statusbar;
    }

    virtual void refresh()
    {
        Refresh(FALSE);
    }

	DECLARE_EVENT_TABLE()
};

class Frame : public wxFrame
{
private:
    GLCanvas *		m_canvas;

public:
	Frame(wxString const & title,
          wxPoint const & pos,
          wxSize const & size)
        : wxFrame((wxFrame *) NULL, -1, title, pos, size)
    {
        m_canvas = new GLCanvas(this);

        // menu: file
        wxMenu * menuFile = new wxMenu;
        menuFile->Append(ID_EXIT, wxT("E&xit"));

        // menubar
        wxMenuBar * menuBar = new wxMenuBar;
        menuBar->Append(menuFile, wxT("&File"));
        SetMenuBar(menuBar);

        // status bar
        CreateStatusBar();
        SetStatusText(wxT("Starting ..."));
        m_canvas->set_status_bar(GetStatusBar());
    }

	~Frame() {}

    void OnClose(wxCloseEvent & even)
    {
        m_canvas->stop();
        Destroy();
    }

    void OnExit(wxCommandEvent & event)
    {
        m_canvas->stop();
        Destroy();
    }

	DECLARE_EVENT_TABLE()
};

class BoxPackApp : public wxApp
{
public:
	virtual bool OnInit()
    {
        Frame * frame = new Frame(wxT("BoxPack"),
                                  wxDefaultPosition,
                                  wxSize(600, 500));
        frame->Show(true);
        SetTopWindow(frame);
        return true;
    }
};

IMPLEMENT_APP(BoxPackApp)

// Event Tables
BEGIN_EVENT_TABLE(Frame, wxFrame)
	EVT_MENU(ID_EXIT, Frame::OnExit)
	EVT_CLOSE(Frame::OnClose)
END_EVENT_TABLE()

BEGIN_EVENT_TABLE(GLCanvas, wxGLCanvas)
	EVT_SIZE(GLCanvas::OnSize)
	EVT_PAINT(GLCanvas::OnPaint)
	EVT_ERASE_BACKGROUND(GLCanvas::OnEraseBackground)
	EVT_ENTER_WINDOW(GLCanvas::OnEnterWindow)
	EVT_MOUSE_EVENTS(GLCanvas::OnMouse)
	EVT_TIMER(ID_TIMER, GLCanvas::OnTimer)
END_EVENT_TABLE()


// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:
