// Copyright (c) 2005 Bonsai Software, Inc.  All rights reserved.

#ifndef Puzzle_h__
#define Puzzle_h__

/// @file Puzzle.h
/// @author Ken Sedgwick

#if defined(WIN32)
#include <windows.h>
#endif

#if defined(LINUX)
#include <stdint.h>
#else
typedef unsigned __int64 uint64_t;
#endif

#include <algorithm>
#include <vector>

#include "Box.h"

class Puzzle
{
public:
    Puzzle();

    ~Puzzle();

    void start();

    void stop();

    bool const is_running() const { return m_running; }

    // Returns position counter.
    uint64_t draw() const;

    void solve(); // Called internally.

protected:
    bool place();

    bool traverse(Box * subroot, Box * bp);

    bool fits() const;

private:
    wxMutex mutable		m_pzmutex;
    bool				m_running;
    wxThread *			m_thread;

    Box *				m_container;
    BoxSeq				m_boxes;
    BoxSeq				m_used;
    Box *				m_root;
    uint64_t			m_positions;
};

// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:

#endif // Puzzle_h__
