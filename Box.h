
// Copyright (c) 2005 Bonsai Software, Inc.  All rights reserved.

#ifndef Box_h__
#define Box_h__

/// @file Box.h
/// @author Ken Sedgwick

#if defined(WIN32)
#include <windows.h>
#endif

#include <algorithm>
#include <vector>

struct Box
{
    static double const 	SCALE;
    static unsigned const	SLOP;

    unsigned				m_ord;

    unsigned				m_w;
    unsigned				m_h;
    unsigned				m_d;

    unsigned				m_x;
    unsigned				m_y;
    unsigned				m_z;

    GLfloat					m_r;
    GLfloat					m_g;
    GLfloat					m_b;

    // Boxes attached to us.
    Box *					m_child[3];

    Box(unsigned ord, unsigned w, unsigned h, unsigned d)
        : m_ord(ord)
        , m_w(w), m_h(h), m_d(d)
        , m_x(0), m_y(0), m_z(0)
    {
        // Assign the colors based on initial orientation.
        m_r = double(m_w) / 69.0;
        m_g = double(m_h) / 44.0;
        m_b = double(m_d) / 39.0;

        for (unsigned i = 0; i < 3; ++i)
            m_child[i] = NULL;
    }

    inline void pos(unsigned x, unsigned y, unsigned z)
    {
        m_x = x;
        m_y = y;
        m_z = z;
    }

    inline void flip(unsigned ndx)
    {
        switch (ndx)
        {
        case 0: std::swap(m_w, m_h); break;
        case 1: std::swap(m_h, m_d); break;
        case 2: std::swap(m_w, m_d); break;
        }
    }

    inline bool contains(Box const & i_other)
    {
        return (m_x <= i_other.m_x + SLOP &&
                m_y <= i_other.m_y + SLOP &&
                m_z <= i_other.m_z + SLOP &&
                m_x + m_w + SLOP >= i_other.m_x + i_other.m_w &&
                m_y + m_h + SLOP >= i_other.m_y + i_other.m_h &&
                m_z + m_d + SLOP >= i_other.m_z + i_other.m_d);
    }

    inline bool intersects(Box const & i_other)
    {
        return !(m_x + m_w <= i_other.m_x + SLOP ||
                 i_other.m_x + i_other.m_w <= m_x + SLOP ||
                 m_y + m_h <= i_other.m_y + SLOP ||
                 i_other.m_y + i_other.m_h <= m_y + SLOP ||
                 m_z + m_d <= i_other.m_z + SLOP ||
                 i_other.m_z + i_other.m_d <= m_z + SLOP);
    }

    inline void attach(unsigned face, Box * i_other)
    {
        switch (face)
        {
        case 0:
            i_other->pos(m_x + m_w, m_y, m_z);
            break;
        case 1:
            i_other->pos(m_x, m_y + m_h, m_z);
            break;
        case 2:
            i_other->pos(m_x, m_y, m_z + m_d);
            break;
        }

        m_child[face] = i_other;
    }

    inline void detach(unsigned face)
    {
        m_child[face] = NULL;
    }

    inline bool operator==(Box const & i_o) const
    {
        return m_ord == i_o.m_ord;
    }

    inline bool operator<(Box const & i_o) const
    {
        return m_ord < i_o.m_ord;
    }

    inline unsigned volume() const
    {
        return m_w * m_h * m_d;
    }

    void draw() const;

    void dump();
};

typedef std::vector<Box *> BoxSeq;

// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:

#endif // Box_h__
