// Copyright (c) 2005 Bonsai Software, Inc.  All rights reserved.

/// @file Trackball.cpp

#if defined(WIN32)
#include <windows.h>
#endif

#include <cmath>

#include "Trackball.h"

float const Trackball::BALLSIZE = 0.8f;

//! Copies one vector (3x1) into another.
void
Trackball::vec_copy(float a[3], float v[3])
{
    v[0] = a[0]; v[1] = a[1]; v[2] = a[2];
}

//! scales the specified vector to the length given through s.
void
Trackball::vec_scale(float v[3], float s)
{
    v[0] *= s; v[1] *= s; v[2] *= s;
}

//! Initializes the vector to a null vector.
void
Trackball::vec_zero(float v[3])
{
    v[0] = v[1] = v[2] = 0.0;
}

//! Sets the vector to the specified values.
void
Trackball::vec_set(float v[3], float x, float y, float z)
{
    v[0] = x; v[1] = y; v[2] = z;
}

//! Project an (x,y) pair onto a sphere or radius r or a hyperbolic sheet
//! if we are away from the center of the sphere.
float
Trackball::project_to_sphere(float r, float x, float y)
{
    float d = sqrt(x*x + y*y);
    if (d < r * 0.70710678118654752440)  // 1.0/sqrt(2)
        return sqrt(r*r - d*d); // inside sphere
    // on hyperbola
    float t = r / 1.41421756237309504880f; // sqrt(2)
    return t*t / d;
}

//! Calculates the cross product of two vectors  (a x b = c).
void
Trackball::vec_cross(float a[3], float b[3], float c[3])
{
    float t[3];
    t[0] = a[1] * b[2] - a[2] * b[1];
    t[1] = a[2] * b[0] - a[0] * b[2];
    t[2] = a[0] * b[1] - a[1] * b[0];
    c[0] = t[0];
    c[1] = t[1];
    c[2] = t[2];
}

//! Calculates the dot product of two vectors  (a o b).
float
Trackball::vec_dot(float a[3], float b[3])
{
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

//! Calculates the difference between two vectors (a - b = c)
void
Trackball::vec_sub(float a[3], float b[3], float c[3])
{
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
}

//! Calculates the sum of two vectors (a + b = c)
void
Trackball::vec_add(float a[3], float b[3], float c[3])
{
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
}

//! Calculates the length of the vector v.
float
Trackball::vec_length(float v[3])
{
    return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

//! Normalizes the specified vector.
void
Trackball::vec_normalize(float v[3])
{
    float l = vec_length(v);
    if (l < 1.0e-9) return;
    v[0] /= l;
    v[1] /= l;
    v[2] /= l;
}

//! Normalizes the quaternion.
void
Trackball::quat_normalize(float q[4])
{
    float l = sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
    if (l < 1.0e-9) return;
    q[0] /= l;
    q[1] /= l;
    q[2] /= l;
    q[3] /= l;
}

//! Defines the quaternion according the specified rotation axis
//! and rotation angle.
void
Trackball::axis_to_quaternion(float a[3], float phi, float q[4])
{
    vec_normalize(a);
    phi /= 2.0;
    float sphi = sin(phi);
    q[0] = a[0] * sphi;
    q[1] = a[1] * sphi;
    q[2] = a[2] * sphi;
    q[3] = cos(phi);
}

//! Calculates the rotation quaternion according the distance on the screen
//! and the size of the trackball.
void
Trackball::calc_quat(float q[4], float p1x, float p1y,
                     float p2x, float p2y)
{
    if (p1x == p2x && p1y == p2y)
    { // zero rotation
        vec_zero(q);
        q[3] = 1.0;
        return;
    }
    float p1[3];
    float p2[3];
    float rot_axis[3];
    float d[3];
    vec_set(p1, p1x, p1y, project_to_sphere(BALLSIZE, p1x, p1y));
    vec_set(p2, p2x, p2y, project_to_sphere(BALLSIZE, p2x, p2y));
    vec_cross(p2, p1, rot_axis);
    vec_sub(p1, p2, d);
    float t = vec_length(d) / (2.0f * BALLSIZE);
    // avoid problems with out-of-control values:
    if (t >  1.0) t =  1.0;
    if (t < -1.0) t = -1.0;
    axis_to_quaternion(rot_axis, 2.0f * asin(t), q);
}

//! Calculates the roation matrix out of the specified quaternion.
void
Trackball::build_rot_matrix(float m[4][4], float q[4])
{
    m[0][0] = 1.0f - 2.0f * (q[1] * q[1] + q[2] * q[2]);
    m[0][1] =        2.0f * (q[0] * q[1] - q[2] * q[3]);
    m[0][2] =        2.0f * (q[2] * q[0] + q[1] * q[3]);
    m[0][3] = 0.0f;

    m[1][0] =        2.0f * (q[0] * q[1] + q[2] * q[3]);
    m[1][1] = 1.0f - 2.0f * (q[2] * q[2] + q[0] * q[0]);
    m[1][2] =        2.0f * (q[1] * q[2] - q[0] * q[3]);
    m[1][3] = 0.0f;

    m[2][0] =        2.0f * (q[2] * q[0] - q[1] * q[3]);
    m[2][1] =        2.0f * (q[1] * q[2] + q[0] * q[3]);
    m[2][2] = 1.0f - 2.0f * (q[1] * q[1] + q[0] * q[0]);
    m[2][3] = 0.0f;

    m[3][0] = 0.0f;
    m[3][1] = 0.0f;
    m[3][2] = 0.0f;
    m[3][3] = 1.0f;
}

//! Given two rotation, defined as quaternions, this function calculates
//! the equivalent single rotation.
void
Trackball::add_rot_quats(float a[4], float b[4], float q[4])
{
    float t1[4];
    float t2[4];
    float t3[4];
    float tf[4];

    vec_copy(a, t1); vec_scale(t1, b[3]);
    vec_copy(b, t2); vec_scale(t2, a[3]);
			
    vec_cross(b, a, t3);
    tf[0] = t1[0] + t2[0] + t3[0];
    tf[1] = t1[1] + t2[1] + t3[1];
    tf[2] = t1[2] + t2[2] + t3[2];
    tf[3] = a[3] * b[3] - (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);

    q[0] = tf[0];
    q[1] = tf[1];
    q[2] = tf[2];
    q[3] = tf[3];

    quat_normalize(q);
}

// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:
