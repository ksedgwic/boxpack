```
This is an attempt to solve my Dad's box packing puzzle.

The original puzzle measurements have an inconsistency.

  The volume of container is 317469
  The total of the blocks is 344028

I converted all of the 69 -> 70 because 35 + 35 = 70.

I converted all of the 43 -> 44 because 22 + 22 = 44.

I adjusted the longest dimension of the container till the pieces fit.

The volumes happened to be exact at this point.

The solution:

1        pos:   0,   0,   0     sz:  39,  70,  44
2        pos:  39,   0,   0     sz:  15,  70,  44
3        pos:  54,   0,   0     sz:  30,  35,  44
4        pos:  84,   0,   0     sz:  30,  70,  22
8        pos:  84,   0,  22     sz:  15,  35,  22
6        pos:  99,   0,  22     sz:  15,  70,  22
5        pos:  54,  35,   0     sz:  15,  35,  44
9        pos:  69,  35,   0     sz:  15,  35,  22
7        pos:  69,  35,  22     sz:  30,  35,  22

This solution does solve my Dad's puzzle ...
```
