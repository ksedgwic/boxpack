// Virtual Trackball.
// Implemented by: Gavin Bell, lots if ideas from Thant Tessman and
// the August 1988 issue of Siggraph's "Computer Graphics",
// pp. 121-129.
// Origial code by: David M. Ciemiewicz, Mark Grossman, Henry Moreton,
// Paul Haeberli
// Ported to C++: Mario Konrad

#if defined(WIN32)
#include <windows.h>
#endif

#ifndef Trackball_h__
#define Trackball_h__

/// @file Trackball.h

class Trackball
{
public:
    //! Calculates the rotation quaternion according the distance on the screen
    //! and the size of the trackball.
    static void calc_quat(float q[4], float p1x, float p1y,
                          float p2x, float p2y);

    //! Calculates the roation matrix out of the specified quaternion.
    static void build_rot_matrix(float m[4][4], float q[4]);

    //! Given two rotation, defined as quaternions, this function calculates
    //! the equivalent single rotation.
    static void add_rot_quats(float a[4], float b[4], float q[4]);

private:
    //! Copies one vector (3x1) into another.
    static void vec_copy(float a[3], float v[3]);

    //! scales the specified vector to the length given through s.
    static void vec_scale(float v[3], float s);

    //! Initializes the vector to a null vector.
    static void vec_zero(float v[3]);

    //! Sets the vector to the specified values.
    static void vec_set(float v[3], float x, float y, float z);

    //! Project an (x,y) pair onto a sphere or radius r or a hyperbolic sheet
    //! if we are away from the center of the sphere.
    static float project_to_sphere(float r, float x, float y);

    //! Calculates the cross product of two vectors  (a x b = c).
    static void vec_cross(float a[3], float b[3], float c[3]);

    //! Calculates the dot product of two vectors  (a o b).
    static float vec_dot(float a[3], float b[3]);

    //! Calculates the difference between two vectors (a - b = c)
    static void vec_sub(float a[3], float b[3], float c[3]);

    //! Calculates the sum of two vectors (a + b = c)
    static void vec_add(float a[3], float b[3], float c[3]);

    //! Calculates the length of the vector v.
    static float vec_length(float v[3]);

    //! Normalizes the specified vector.
    static void vec_normalize(float v[3]);

    //! Normalizes the quaternion.
    static void quat_normalize(float q[4]);

    //! Defines the quaternion according the specified rotation axis
    //! and rotation angle.
    static void axis_to_quaternion(float a[3], float phi, float q[4]);

    static const float BALLSIZE;
};

// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:

#endif // Trackball_h__
