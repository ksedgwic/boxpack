// Copyright (c) 2005 Bonsai Software, Inc.  All rights reserved.

/// @file Box.cpp
/// @author Ken Sedgwick

#if defined(WIN32)
#include <windows.h>
#endif

#include <cmath>
#include <iostream>
#include <iomanip>

#include <GL/gl.h>
#include <GL/glu.h>

#if defined(WIN32)
#include <wx/msw/winundef.h>
#endif

#include "wx/wx.h"
#include "wx/app.h"
#include "wx/thread.h"
#include "wx/event.h"
#include "wx/glcanvas.h"

#include "Box.h"
#include "Puzzle.h"

using namespace std;

double const Box::SCALE = 0.08;
unsigned const Box::SLOP = 0;	// Use 0 for exact matching.

void
Box::draw() const
{
    float w = double(m_w) * SCALE;
    float h = double(m_h) * SCALE;
    float d = double(m_d) * SCALE;

    float x = double(m_x) * SCALE;
    float y = double(m_y) * SCALE;
    float z = double(m_z) * SCALE;

    glPushMatrix();
    glTranslatef(x, y, z);

    glColor4f(m_r, m_g, m_b, 1.0);

    glPolygonMode(GL_FRONT, GL_FILL);
    glPolygonMode(GL_BACK, GL_POINT);

    glBegin(GL_QUADS);

    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, h,   0.0);
    glVertex3f(  w, h,   0.0);
    glVertex3f(  w, 0.0, 0.0);

    glVertex3f(  w, 0.0,   d);
    glVertex3f(  w, h,     d);
    glVertex3f(0.0, h,     d);
    glVertex3f(0.0, 0.0,   d);

    glVertex3f(  w, 0.0, 0.0);
    glVertex3f(  w, 0.0,   d);
    glVertex3f(0.0, 0.0,   d);
    glVertex3f(0.0, 0.0, 0.0);

    glVertex3f(0.0,   h, 0.0);
    glVertex3f(0.0,   h,   d);
    glVertex3f(  w,   h,   d);
    glVertex3f(  w,   h, 0.0);

    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0,   d);
    glVertex3f(0.0,   h,   d);
    glVertex3f(0.0,   h, 0.0);

    glVertex3f(  w,   h, 0.0);
    glVertex3f(  w,   h,   d);
    glVertex3f(  w, 0.0,   d);
    glVertex3f(  w, 0.0, 0.0);

    glEnd();

    glColor4f(0.0, 0.0, 0.0, 1.0);
    glLineWidth(1.5);
    
    glBegin(GL_LINES);

    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(  w, 0.0, 0.0);

    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0,   h, 0.0);

    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0,   d);

    glVertex3f(  w, 0.0, 0.0);
    glVertex3f(  w,   h, 0.0);

    glVertex3f(  w, 0.0, 0.0);
    glVertex3f(  w, 0.0,   d);

    glVertex3f(0.0,   h, 0.0);
    glVertex3f(  w,   h, 0.0);

    glVertex3f(0.0,   h, 0.0);
    glVertex3f(0.0,   h,   d);

    glVertex3f(0.0, 0.0,   d);
    glVertex3f(  w, 0.0,   d);

    glVertex3f(0.0, 0.0,   d);
    glVertex3f(0.0,   h,   d);

    glVertex3f(  w,   h, 0.0);
    glVertex3f(  w,   h,   d);

    glVertex3f(  w, 0.0,   d);
    glVertex3f(  w,   h,   d);

    glVertex3f(0.0,   h,   d);
    glVertex3f(  w,   h,   d);

    glEnd();
    glPopMatrix();

    // Draw the children
    for (unsigned i = 0; i < 3; ++i)
        if (m_child[i])
            m_child[i]->draw();
}

void
Box::dump()
{
    cerr << m_ord << '\t'
         << right << setw(3) << setfill(' ')
         << " pos: " << right << setw(3) << setfill(' ') << m_x
         << ", "     << right << setw(3) << setfill(' ') << m_y
         << ", "     << right << setw(3) << setfill(' ') << m_z
         << "\tsz: "  << right << setw(3) << setfill(' ') << m_w
         << ", "     << right << setw(3) << setfill(' ') << m_h
         << ", "     << right << setw(3) << setfill(' ') << m_d
         << endl;

    for (unsigned i = 0; i < 3; ++i)
        if (m_child[i])
            m_child[i]->dump();
}

// Local Variables:
// mode: C++
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets: ((comment-intro . 0))
// End:
